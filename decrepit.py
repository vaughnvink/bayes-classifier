import re
from os import listdir
from os.path import isfile, join
Document, DocBase,

def readText(fileName):
    text_file = open(fileName, 'r')
    text = text_file.read()
    text = re.sub(r'[^\w]', ' ', text)
    text = re.sub(r'\s+', ' ', text).lower()
    textDB = text.split(" ")
    textDB.pop()
    text_file.close()
    return textDB

def fetchFiles(path):
    return [f for f in listdir(path) if isfile(join(path, f))]

def constructDataset():
    documents = []
    for file in fetchFiles(Settings[1]):
        for category in Settings[0]:
            if bool(re.search(category[1], file)):
                documents.append(Document(file, readText(Settings[1]+"\\"+file), category[0]))
    return DocBase(documents, Settings[0])

## Construct Dataset object
DB = constructDataset()

## Run functions on Dataset
#DB.loadChiSquareList("corpus1.txt")
#print(DB.classify(300,readText(Settings[1]+"\\spmsga126.txt")))

#txt = readText("corpus-mails\\corpus\\part10\\spmsgc89.txt")
#print(txt)
#print(DB.calcWordGivenCategorySmoothed("ham","free"))
#print(DB.calcProbability("spam",300,txt))
#print(DB.calcProbability("ham",300,txt))
#print(DB.classify(300,txt))
#DB.calcProbability("ham",100,readText(Settings[1]+"\\spmsga128.txt"))
#print(DB.sortChiSquareList())
#DB.saveChiSquareList("chiSorted.txt")
#T.knowledgeBase.saveChiSquareList("combinedCorpus.txt")
#print("Result "+ str(T.testDirectory(300)))