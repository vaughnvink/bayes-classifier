from supportingClasses import Tester
import time

Settings = [
    [   # Setup of categories [<Name of category>,<Filename contains regex pattern>]
        ["spam","spmsga"],
        ["ham","-"]
    ],  # Path where the training data is located
        "corpus-mails\\corpus\\part1-9",
        # Path where the testing data is located
        "corpus-mails\\corpus\\part10"
]


### Start timer ###
start_time = time.time()

### Create tester class - (Categories with identifiers, training folder path, testing folder path, [optional] loadable ChiSquare list)
T = Tester(Settings[0], Settings[1], Settings[2])

### Classifier - (ChiSquare range,File path)
#T.classify(100,Settings[1]+"\\spmsga126.txt")

### Accuracy calculator - (ChiSquare range)
T.testDirectory(1)

### Print time passed ###
print("--- %s seconds ---" % (time.time() - start_time))





