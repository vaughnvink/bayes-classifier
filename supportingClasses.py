import re
from os import listdir
from os.path import isfile, join

#Class which holds a word (from a file) and its chi-square value
class Word:
	def __init__( self, text, chiSquare):
		self.text = text
		self.chiSquare = chiSquare
	def __str__(self):
		return self.text

	#formats the word and chi-square in a nice way.
	def print( self ):
		return self.text+","+str(self.chiSquare)

#Class which represents a file
class Document:
	def __init__( self, name, words, category):
		self.name = name
		self.words = words
		self.category = category

	#true/false for if a certain word exists within a document
	def contains( self, text ):
		if any(text in s for s in self.words):
			return 1
		return 0

#Class that contain all low-level calculations for both chi-squares and regular probabilities
class DocBase:
	def __init__( self, documents, categories):
		self.documents = documents
		self.categories = categories
		self.cache = [0]
		self.chiSquares = []

	#Calculates Pˆ(w|c) (formula from assignment introduction)
	def calcWordGivenCategorySmoothed( self, category, word ):
		return (self.contains(category,word) + 1) / (self.contains(category) + 2)

	#Calculates a single probability
	def calcProbability( self , category, vocabRange, words):
		probability = self.contains(category)/len(self.documents)
		for i in range(0,vocabRange):
			if self.chiSquares[i].text in words:
				probability *= self.calcWordGivenCategorySmoothed(category,self.chiSquares[i].text)
			else:
				probability *= 1-self.calcWordGivenCategorySmoothed(category, self.chiSquares[i].text)
		print("--> "+str(probability))
		return probability

	#Classifies a file depending on loaded categories
	def classify( self, vocabRange, words):
		print("[calcCategoryProbability] Starting classifier")
		cateList = []
		for categoryEntry in self.categories :
			cateList.append([categoryEntry[0],self.calcProbability(categoryEntry[0], vocabRange,words)])
		maximum = sorted(cateList,key=lambda x: x[1],reverse=True)[0]
		print("[calcCategoryProbability] File is likely " + str(maximum[0]))
		return maximum

	#Loads the words from a document
	def extractWords( self ):
		words = []
		for document in self.documents:
			for word in document.words:
				words.append(word)
		return list(set(words))

	#Returns amount of occurrences of a word, optionally within a category
	def contains( self, category, word = ""):
		x = 0
		for document in self.documents:
			if document.category == category:
				if word != "":
					if document.contains(word):
						x += 1
				else:
					x += 1
		return x

	#Returns the expected value E(i,j) (formula from assignment introduction)
	def expected( self, searchCategory, word ):
		totalWordCategory = 0
		for category in self.categories:
			totalWordCategory+=self.contains(category[0],word)
		return totalWordCategory*self.contains(searchCategory)/self.cache[0]

	#Returns the expected value E(i,j) inverse
	def negExpected( self, searchCategory, word ):
		totalWordCategory = 0
		for category in self.categories:
			totalWordCategory+=self.cache[self.categories.index(category)+1]-self.contains(category[0],word)
		return totalWordCategory*self.contains(searchCategory)/self.cache[0]

	#Caches categories to prevent runtime for rising exponentially
	def cacheCategories( self ):
		for category in self.categories:
			temp = self.contains(category[0])
			self.cache[0] += temp
			self.cache.append(temp)

	#Sorts the class' chi-squares
	def sortChiSquareList( self ):
		self.chiSquares = sorted(self.chiSquares,key=lambda x: x.chiSquare,reverse=True)

	#Loads a chi-square list file into memory
	def loadChiSquareList( self ,fileName):
		print("[loadChiSquareList] Loading ChiSquares from "+fileName+"...")
		text_file = open(fileName, 'r')
		text = text_file.read()
		temp = text.split(",")
		for index in range(0,len(temp),2):
			print("["+str(int(index/2+1))+"/"+str(int(len(temp)/2))+"]")
			self.chiSquares.append(Word(temp[index],temp[index+1]))
		text_file.close()
		print("[loadChiSquareList] Finished loading!")

	#Saves a chi-square list to a file, called after generating
	def saveChiSquareList( self ,fileName):
		text_file = open(fileName, "w+")
		writeable = ""
		for word in self.chiSquares:
			writeable+=(word.print()+",")
		writeable = writeable[:-3]
		text_file.write(writeable)
		text_file.close()
		print("[saveChiSquareList] Saved chiList!")

	#Generates a chi-square list, then sorts and saves it.
	def generateChiSquareList( self, fileName = ""):
		print("[generateChiSquareList] Starting ChiSquare calculation...")
		self.cacheCategories()
		extracted = self.extractWords()
		x=0
		for word in extracted:
			x+=1
			print("["+str(x)+"/"+str(len(extracted))+"] chiSquareComplete " + word)
			self.chiSquares.append(Word(word,self.chiSquareComplete(word)))
		print("[generateChiSquareList] Finished ChiSquaring!")
		self.sortChiSquareList()
		if fileName != "":
			self.saveChiSquareList(fileName)
		return self.chiSquares

	#Combines partial chi-squares from multiple categories into one
	def chiSquareComplete( self , word):
		total = 0
		for category in self.categories:
			total+=self.chiSquare(category[0],word)
			total+=self.negChiSquare(category[0], word)
		print("--> " + str(total))
		return total

	#Returns a single chi-square for i,j
	def chiSquare( self, searchCategory, word ):
		Exp = self.expected(searchCategory, word)
		if Exp == 0:
			print("[EXPFix]")
			return 0
		return pow(self.contains(searchCategory,word)-Exp,2)/Exp

	#Returns a single chi-square for i,j inverse
	def negChiSquare( self, searchCategory, word ):
		Exp = self.negExpected(searchCategory, word)
		if Exp == 0:
			print("[EXPFix]")
			return 0
		return pow(self.contains(searchCategory)-self.contains(searchCategory,word)-Exp,2)/Exp

#Class which handles the testing and automation of calls required to the DocBase (calculator)
class Tester:
	def __init__(self, categories, corpusPath, testingPath, chiPath="default.txt"):
		self.corpusPath = corpusPath
		self.testingPath = testingPath
		self.categories = categories
		self.knowledgeBase = DocBase(self.constructDataset(corpusPath),categories)
		if chiPath!="default.txt":
			self.knowledgeBase.loadChiSquareList(chiPath)
		else:
			self.knowledgeBase.generateChiSquareList(chiPath)
		self.testFiles = self.constructDataset(testingPath)

	#Reads a file and sanitises it
	def readText( self, fileName ):
		text_file = open(fileName, 'r')
		text = text_file.read()
		text = re.sub(r'[^\w]', ' ', text)
		text = re.sub(r'\s+', ' ', text).lower()
		textDB = text.split(" ")
		textDB.pop()
		text_file.close()
		return textDB

	#Returns a list of all files within a directory
	def fetchFiles( self, path ):
		return [f for f in listdir(path) if isfile(join(path, f))]

	#Builds a list of documents, with categories
	def constructDataset( self , path):
		documents = []
		for file in self.fetchFiles(path):
			for category in self.categories:
				if bool(re.search(category[1], file)):
					documents.append(Document(file, self.readText(path + "\\" + file), category[0]))
		return documents

	#Calls lower-down classify function from the DocBase class
	def classify( self, vocabRange, path ):
		return self.knowledgeBase.classify(vocabRange,self.readText(path))

	#Tests an entire directory and returns the accuracy
	def testDirectory( self, vocabRange ):
		correct = 0
		x = 0
		for file in self.testFiles:
			x+=1
			print("[" + str(x) + "/" + str(len(self.testFiles)) + "] testDirectory " + file.name)
			result = self.knowledgeBase.classify(vocabRange,file.words)
			if result[0] == file.category:
				correct+=1
		corLength = correct/len(self.testFiles)
		print(str(corLength*100) + "% correct")
		return correct/len(self.testFiles)
